let array = []
for (let i = 0; i < 1000000; i += 1){
    array.push(1)
}

console.time('loop1')
for (var i = 0; i < array.length; i += 1){
    // Do nothing
}
console.timeEnd('loop1')

console.time('loop2')
for (let i = 0; i < array.length; i += 1){
    // Do nothing
}
console.timeEnd('loop2')


