
const databaseCall = ()=>{
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{console.log('Diuracio de la llamada a la base')},2000)
        resolve(['John','Pedro'])
    })
}
const printUsers=(users)=>{
    console.log(users)
}
const getUsers =()=>{
    let users =  databaseCall();
    console.log('Estos son todos los usuarios');
    printUsers(users)
    console.log('Termina el proceso de impresión')
}

getUsers();
//Evitar el problema de variable indefinida